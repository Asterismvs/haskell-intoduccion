# Introducción Básica a Haskell

## Funcionalmente Haskell

Haskell es un lenguaje de programación puramente funcional.
Esto significa que no le decimos a la computadora lo que tiene que hacer, sino como son las cosas.

El factorial de un número es el producto de todos los números desde el 1 hasta ese número.

La suma de una lista de números es el primer número más la suma del resto de la lista.

Todo se expresa como funciones, no por nada es puramente funcional xD

Si establecemos una variable, por ejemplo:

```haskell
a = 7
```

Esta variable siempre sera _7_

Esto produce que cualquier función carezca de efectos secundarios y daños colaterales. La función calcula y devuelve el resultado.

Si una función es llamada dos veces con los mismos parámetros, tendremos como resultado siempre el mismo.

Esto se llama, **TRANSPARENCIA REFERENCIAL.**

Lo cual permite al compilador razonar acerca del comportamiento del programa, y nos permite deducir fácilmente que una función es correcta, para así poder construir funciones más complejas.

### Características Lenguaje Funcional

- ⁂ Pure (mathematical) functions
- ⁂ Inmutable data
- ⁂ No/Less side-effects
- ⁂ Declarative
- ⁂ Easier to verify

---

## Haskell no es un oso, mas sí perezoso

Haskell no hará nada a menos que realmente lo tenga que hacer.

La ejecución de funciones y el calculo de resultados, quedan latentes hasta que se vea forzado a hacerlo.

Junto a la _transparencia referencial_, permite que veamos los programas como una serie de transformaciones de datos. Hasta nos permite jugar con cosas tan divertidas como estructuras de datos infinitas.

Por ejemplo, tenemos una lista de números inmutables:

```haskell
xs = [1,2,3,4,5,6,7,8]
```

Y una función `doubleMe` que multiplica cada elemento por dos, y devuelve una nueva lista.

En un lenguaje imperativo, si queremos multiplicar nuestra lista por 8, e hiciéramos `doubleMe(doubleMe(doubleMe(xs)))`, seguramente se recorrería la lista, haría una copia y devolvería un valor. El siguiente paso sería recorrerla dos veces más a la lista y devolver el valor final.

En un lenguaje "lazy", llamar a `doubleMe` con una una lista sin forzar ao que muestre el valor, el codigo te comenta "Pa'que lo hago si no hace falta?".
Si quisiese ver el resultado, el primer `doubleMe` le dice al segundo que quiere el resultado en este momento. El segundo `doubleMe` le dice lo mismo al tercero, que este un poco indignado te devuelve un `1` duplicado, lo que comúnmente se conoce como `2`. El segundo lo recibe y le da al primero un `4`, el cual lo toma y anuncia a viva voz que el primer elemento de la lista es un `8`.
Si se necesita, se hace, sino no.

Veámoslo con una comparación con un lenguaje diferente, en este caso, Java:

### Imperative

```java

int sum(int arr, int length) {

 int i, sum = 0;

 for(i = 0; i < length; i++) {

  sub += arr[i];
 }

 return sum;
}

```

Le decís al programa paso por paso que tiene que hacer.
Le decimos que settee `sum` a 0, al igual que `i`.
Luego iterar hasta que cierta condición se cumple.
Toma cada elemento o valor individual del array, que depende de `i`, y lo añade a `sum` y al terminar lo retorna.
En este caso no le damos al computador una definición de que significa `sum` o lo que `sum` actualmente es.
Solamente describe un algoritmo que puede producir `sum`

### Declarative

```haskell

sum[] = 0
sum(x:xs) = x + sum xs

-- O si no

sum x = foldr (+) 0 x

```

En un enfoque declarativo como en Haskell,
definimos, o más o menos definimos lo que significa tener `sum`,
asi que le decimos que `sum` de una lista vacía es 0.
Y que `sum` de una lista con al menos un elemento `x`,
es el elemento `x` sumado a `sum` del resto.

## Evaluaciones perezosas y estrictas

### Strict

```java

int fun(int arg) {

 int x = func1(arg);
 int y = func2(arg);
 int z = func3(arg);

 if(z) {
  return x;
 } else {
  return y;
 }
}

```

Pongamos el supuesto de que cada una de las funciones `func#`, tardan un año en procesarse.
En este ejemplo "estricto", las variables `x, y, z` tienen por valor el resultado de una función.
Para asignar cada valor a cada variable, el programa necesita evaluar cada una de las funciones `func#`.
Lo cual nos daría una duración de tres años (un año cada una).
Luego el `if...else` procedería rápidamente, para retornar el valor correcto

### Lazy

```haskell

func arg =
 let x = func1 arg
  y = func2 arg
  z = func3 arg
 in
 if z then x else y

```

En los casos de lenguajes "lazy", el programa no procesa nada hasta que no sea estrictamente necesario.
Por ejemplo, `x,y,z` están definidas por el resultado de ciertas funciones, las cuales no se evalúan según el orden del código, sino que RECIÉn se evalúan al necesitar sus valores en el estamento `if...else`.
Primero se evalúa `z`, y en función de su valor, el programa decide cual de las otras variables restantes evaluar para retornar.
